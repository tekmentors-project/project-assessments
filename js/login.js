$(document).ready(function () {



// $(".pickTemp").click(function(event){
// event.preventDefault();
// createAssesement().then( () => {
// console.log("got assessment id");
// window.location = 'quizEditor.html';
// });


// });


});


(function () {

    
    const newEmail=document.getElementById("newEmail");
    const newPassword=document.getElementById("newPassword");

    var signInButton = document.getElementById('sign-in-button');

    signInButton.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider);
      });

    //Add Login Event
    btnLogin.addEventListener('click', e => {
        //Get email and password
        const email = txtEmail.value;
        const pass = txtPassword.value;
        const auth = firebase.auth();

        //Sign In
        const promise=auth.signInWithEmailAndPassword(email, pass);
        promise.catch(e=>console.log(e.message));
    });

     //Add SignUp Event
     btnSignUp.addEventListener('click', e => {
        //Get email and password
        
        const email = newEmail.value;
        const pass = newPassword.value;
        const auth = firebase.auth();

        //Sign Up
        const promise=auth.createUserWithEmailAndPassword(email, pass);
        promise.catch(e=>console.log(e.message));
    });

    //Siging Out
    btnLogout.addEventListener('click', e=> {
        firebase.auth().signOut();
        window.location = 'home.html';
    });

    //Add a realtime listener
    firebase.auth().onAuthStateChanged(firebaseUser =>{
        if(firebaseUser){
            console.log(firebaseUser);
            $("#btnLogout").removeClass("d-none");
            $("#results").removeClass("d-none");
            $("#registration").addClass("d-none");
            $('#displayContent').load('templatesHome.html', function () {
                $('.carousel-main').carousel();
                loaded();
            });
        }
        else{
            console.log('not logged in');
            $("#btnLogout").addClass("d-none");
            $("#results").addClass("d-none");
            $("#registration").removeClass("d-none");
        }
    })

    function loaded() {
        $("#linkToTemp1").bind('click', function () {
            var user = firebase.auth().currentUser;
            console.log(user);
            if (user) {
                createAssesement().then(() => {
                    window.location = 'quizEditor.html';
                    console.log("got assessment id");
                });
            } else {
                $('#displayContent').load('login.html');
            }
        });
    }


}());




