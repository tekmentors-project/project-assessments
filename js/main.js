$(document).ready(function () {
	$('#main-editor').load('template.html', function () {
		$('.carousel-main').carousel();
		edtClass();
		edtImg();
		saveImg();
		saveBgImg();
		saveResOptImg();
		saveQues();
		deleteQues();
		updateQuesText();
		removeEditImg();
		setWgt();
	})
	//global variable
	var clkEdtWeight;
	var assmntId;
	var questionArray = [];
	var newQuesID = 1;

	//Content Editable Border show Hide
	var edtClass = function () {
		$("*[contenteditable='true']").bind("mouseover", function () {
			$(this).addClass("editBorder");
		});

		$("*[contenteditable='true']").bind("mouseout", function () {
			$(this).removeClass("editBorder");
		});
	}

	$('.btn-goLive').bind("click", function () {
		alert('https://assasement-1535346917536.firebaseapp.com/assesement-' + localStorage.getItem('assesementId'));
	});


	var updateQuesText = function () {
		$("#main-editor>.content>.ques-div>h4[contenteditable='true']").bind("mouseout", function () {
			qID = $(this).parent().parent().attr('id');
			navID = '#nav-item-' + qID.substring('list-item-'.length, qID.length);
			$(navID).text($(this).text());
		});

	}

	var setWgt = function () {
		$('.optwgt').bind("click", function () {
			$(this).next().click();
		});
	}


	//Content Editable Image
	var deleteQues = function () {
		$(".btn-delete-ques").bind("click", function () {
			qID = $(this).parent().attr('id');
			navID = '#nav-item-' + qID.substring('list-item-'.length, qID.length);
			$(navID).remove();
			$(this).parent().remove();
		});
	}

	//Content Editable Image
	var edtImg = function () {
		$(".fa-pencil").bind("click", function () {
			$(this).prev().val("");
			$(this).prev().removeClass("d-none");
			$(this).next().removeClass("d-none");
			$(this).next().next().removeClass("d-none");
			$(this).addClass("d-none");
			$(this).parent().prev().css('opacity', '0.6');
		});
	}

	//Remove Content Editable div
	var removeEditImg = function () {
		$(".fa-times").bind("click", function () {
			$(this).prev().removeClass("d-none");
			$(this).next().addClass("d-none");
			$(this).prev().prev().addClass("d-none");
			$(this).addClass("d-none");
		});
	}

	//Save all question
	var saveQues = function () {
		$(".btn-save-ques").bind("click", function () {
			assmntId = localStorage.getItem('assesementId');
			questionArray = [];
			var desc = getAssesmentDesc();
			var optArray = getAssesmentOptions();
			var imgg = getAssesmentImg();
			var resultHeading = getResultHeading();
			$("#main-editor>div:not('.d-none'):not('#list-item-Welcome'):not('#list-item-resultOpt')").each(function () {
				publishQues($(this));
			});
			populateQuestion(assmntId, desc, imgg, optArray, resultHeading, questionArray);
		});
	}

	//Save Content Editable Image
	var saveImg = function () {
		$(".saveImg").bind("click", function () {
			$(this).parent().prev().attr("src", $(this).parent().find('.pnc').val());
			$(this).addClass("d-none");
			$(this).prev().prev().prev().addClass("d-none");
			$(this).prev().addClass("d-none");
			$(this).prev().prev().removeClass("d-none");
			$(this).parent().prev().css('opacity', '1.0');
		});
	}

	//Save Content Editable Assesment Backgorund Image
	var saveBgImg = function () {
		$(".saveBgImg").bind("click", function () {
			$('.content').css('background-image', 'url("' + $(this).parent().find('.pnc').val() + '")');
			$(this).addClass("d-none");
			$(this).prev().prev().prev().addClass("d-none");
			$(this).prev().addClass("d-none");
			$(this).prev().prev().removeClass("d-none");
		});
	}

	//Save Image of Result Title
	var saveResOptImg = function () {
		$('.saveResultOpt').bind("click", function () {
			$(this).parent().parent().find('.card-img-top').attr("src", $(this).parent().find('.pnc').val());
			$(this).addClass("d-none");
			$(this).prev().prev().prev().addClass("d-none");
			$(this).prev().addClass("d-none");
			$(this).prev().prev().removeClass("d-none");
		});
	}

	//Get Assesment Description and heading
	var getAssesmentDesc = function () {
		return $("#list-item-Welcome>div>h1").text();
	}

	//Get Assesment Description and heading
	var getResultHeading = function () {
		return $("#list-item-resultOpt>div>h1").text();
	}

	//Get assessment valid result options
	var getAssesmentOptions = function () {
		var validOptions = [];
		$("#list-item-resultOpt").find(".card-div").each(function () {
			var validOptTxt = $(this).find(".card-title").text();
			var validOptDesc = $(this).find(".card-text").text();
			var validOptImg = $(this).find(".card-img-top").attr('src');
			validOptions.push(new validOption(validOptTxt, validOptDesc, validOptImg));
		});
		return validOptions;
	}


	var getAssesmentImg = function () {
		return $('.content').css('background-image').replace('url(', '').replace(')', '');
	}

	//Get Assesment Description and heading
	var getAssesmentDesc = function () {
		return $("#list-item-Welcome>div>h1").text();
	}

	//Get Assesment Description and heading
	var getResultHeading = function () {
		return $("#list-item-resultOpt>div>h1").text();
	}

	//Get assessment valid result options
	var getAssesmentOptions = function () {
		var validOptions = [];
		$("#list-item-resultOpt").find(".card-div").each(function () {
			var validOptTxt = $(this).find(".card-title").text();
			var validOptDesc = $(this).find(".card-text").text();
			var validOptImg = $(this).find(".card-img-top").attr('src');
			validOptions.push(new validOption(validOptTxt, validOptDesc, validOptImg));
		});
		return validOptions;
	}


	var getAssesmentImg = function () {
		return $('.content').css('background-image').replace('url(', '').replace(')', '');
	}

	edtClass();
	edtImg();
	saveImg();
	saveBgImg();
	saveResOptImg();
	saveQues();
	deleteQues();
	updateQuesText();
	removeEditImg();
	setWgt();

	// Add new question
	$("#addQues").click(function () {

		$("#list-item-x").clone().appendTo("#main-editor");
		$("#main-editor>div:last-child").removeClass("d-none");
		$("#nav-item-x").clone().insertBefore($("#addQues"));
		$("#list-example a:nth-last-child(2)").removeClass("d-none");
		var id1 = "list-item-" + newQuesID;
		var id2 = "nav-item-" + newQuesID;
		$('#main-editor>div:last-child').attr('id', id1);
		$('#list-example a:nth-last-child(2)').attr('id', id2);
		$('#list-example a:nth-last-child(2)').attr('href', '#' + id1);
		$('#list-example a:nth-last-child(2)')[0].click();
		newQuesID++;
		edtClass();
		edtImg();
		saveImg();
		deleteQues();
		updateQuesText();
		removeEditImg();
		setWgt();
	});



	$(".btn-createAssesment").click(function () {
		createAssesement();
		/*if(localStorage.getItem('assesementId')!=null){
			$(".btn-save-ques").removeAttr("disabled");    
		}*/
	});



	var publishQues = function (ques) {
		assmntId = localStorage.getItem('assesementId');
		if (assmntId != null) {
			var quesTxt = ques.find("div>h4").text().trim();
			var quesImg = ques.find(".ques-pic>img").attr('src').trim();
			var options = [];

			ques.find(".ques-radio>div").each(function () {
				var optTxt = $(this).find("label").text();
				var v = '';
				$.each($(this).find(".optwgt").val().split(','), function (index, value) {
					v = v + value.split(':')[1] + ',';
				});
				var optWgt = '[' + v.substring(0, v.length - 1) + ']';
				options.push(new option(optTxt, optWgt));
			});

			var obj = new Question(quesTxt, quesImg, options);
			questionArray.push(obj);
		}
	}


	$("ques-div").click(function () {

		$("#list-item-x").clone().appendTo("#main-editor");
		$("#main-editor>div:last-child").removeClass("d-none");
		$("#nav-item-x").clone().insertBefore($("#addQues"));
		$("#list-example a:nth-last-child(2)").removeClass("d-none");
		var id = $("#main-editor .content").length - 2;
		var id1 = $("#main-editor>div:last-child").attr('id').replace("-x", "-" + id);
		var id2 = $("#list-example a:nth-last-child(2)").attr('id').replace("-x", "-" + id);
		$('#main-editor>div:last-child').attr('id', id1);
		$('#list-example a:nth-last-child(2)').attr('id', id2);
		$('#list-example a:nth-last-child(2)').attr('href', '#' + id1);


	});

	$('#wgtModal').on('show.bs.modal', function (event) {
		var res = $(".res");
		$(".wtLabel").each(function (index) {
			$(this).text(trimSpaces(res[index].innerHTML));
		});
		clkEdtWeight = $(event.relatedTarget);

		if (clkEdtWeight.prev().val() != 'undefined' && clkEdtWeight.prev().val() != '') {
			var wt = clkEdtWeight.prev().val().split(",");
			$('#quant1').val(wt[0].split(':')[1].trim());
			$('#quant2').val(wt[1].split(':')[1].trim());
			$('#quant3').val(wt[2].split(':')[1].trim());
			$('#quant4').val(wt[3].split(':')[1].trim());
		}

		else {
			$('#quant1').val(1);
			$('#quant2').val(1);
			$('#quant3').val(1);
			$('#quant4').val(1);
		}

	});




	//Save weightage in modal
	$('#save-wgt').click(function () {
		var res = $(".res");
		var str = trimSpaces(res[0].innerHTML) + ':' + $('input[name=quant1]').val() + ',' + trimSpaces(res[1].innerHTML) + ':' + $('input[name=quant2]').val() + ',' + trimSpaces(res[2].innerHTML) + ':' + $('input[name=quant3]').val() + ',' + trimSpaces(res[3].innerHTML) + ':' + $('input[name=quant4]').val();
		clkEdtWeight.prev().val(str);
		$('#wgtModal').modal('hide');
		$('.modal-backdrop').remove();
	});





	// var saveBgImg = function () {
	// 	$(".saveBgImg").bind("click", function () {
	// 		$('.content').css('background-image', 'url("' + $(this).parent().find('.pnc').val() + '")');
	// 		$(this).addClass("d-none");
	// 		$(this).prev().prev().prev().addClass("d-none");
	// 		$(this).prev().addClass("d-none");
	// 		$(this).prev().prev().removeClass("d-none");
	// 	});
	// }


	function trimSpaces(val) {
		return val.replace(/&(nbsp|#160);/gi, ' ').replace(/\s\s+/g, ' ').trim();
	}
});
