//1. Add Question to a assesement in Firebase, with incremental assesementId ... get it from assesementCounter
//2. Add Result to Firebase , with the assesmentId of the current assesement
//3. Get Question from Database

// Initialize Firebase
var config = {
    apiKey: "AIzaSyBFfh8OOOolD5vy_SgWUtlZbzfVw_wslPE",
    authDomain: "assasement-1535346917536.firebaseapp.com",
    databaseURL: "https://assasement-1535346917536.firebaseio.com",
    projectId: "assasement-1535346917536",
    storageBucket: "assasement-1535346917536.appspot.com",
    messagingSenderId: "342781541633"
};
firebase.initializeApp(config);

var assesementCounter = 1;
var db_ref = firebase.database();

db_ref.ref("/assesementCounter").once('value', function (snapshot) {
    assesementCounter = snapshot.val();
});

function Question(questionText, questionImage, options) {
    this.questionText = questionText;
    this.questionImage = questionImage;
    this.options = options;
}

// On creation of Landing Page
function createAssesement() {

    return new Promise(function (resolve, reject) {

        db_ref.ref("/assesement/assesementCounter").once('value').then(function (snapshot) {
            var jsonObj = {};
            var assesementCounter = snapshot.val();
            assesementCounter++;
            db_ref.ref("/assesement/assesementCounter").set(assesementCounter);
            jsonObj.assesementId = assesementCounter;
            console.log("AssesementId: " + assesementCounter);
            localStorage.setItem('assesementId', assesementCounter);

            return resolve();
        });

    });
}

var option = function (text, weightage) {
    this.text = text;
    this.weightage = weightage;
}

var validOption = function (name, description, image) {
    this.name = name;
    this.description = description;
    this.image = image;
}

//Saves question to DataBase
function populateQuestion(assesementId, assesementDesc, assesementImage, validOptions, resultHeading, questions) {

    var assesement = {};
    assesement.assesementId = assesementId;
    assesement.assesemenDesc = assesementDesc;
    assesement.assesementImage = assesementImage;
    assesement.validOptions = validOptions;
    assesement.resultHeading = resultHeading;

    db_ref.ref("/assesement/" + assesementId).update(assesement);
    console.log(questions);
    db_ref.ref("/assesement/" + assesementId + "/questions").update(questions);

}


// https://assasement-1535346917536.firebaseio.com/assesement/1/questions.json?shallow=true

function getAssesement(assesementId) {
    const dbPath = `https://assasement-1535346917536.firebaseio.com/assesement/${assesementId}.json`;
    return new Promise((resolve, reject) => {
        $.ajax({
            url: dbPath,
            type: 'GET',
            contentType: 'text/plain',
            dataType: 'json',
            success: function (data) {
                resolve(data);

            },
            error: function (error) {
                reject(error);
            }
        });
    });
}

function saveResult(assesementId, userName, userEmail, assesResult) {
    var resultObj = {};
    resultObj.assesemenId = assesementId;
    resultObj.userName = userName;
    resultObj.userEmail = userEmail;
    resultObj.assesResult = assesResult;

    saveAssesementResult(resultObj).then((data) => {
        console.log(data);
    })
}

function saveAssesementResult(resultObj) {
    const dbPath = `https://assasement-1535346917536.firebaseio.com/result.json`;
    return new Promise((resolve, reject) => {
        $.ajax({
            url: dbPath,
            type: 'POST',
            data: JSON.stringify(resultObj),
            contentType: 'text/plain',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                resolve(data);
            },
            error: function (error) {
                console.log(error)
            }
        });
    });
}

window.onload = function getData() {
    try {
        var url = window.location.href.split("-");
        var assesementId = url[url.length - 1];
        console.log("assesementId : " + assesementId);
        if (parseInt(assesementId)) {
            getAssesement(assesementId).then(
                data => {
                    displayAssesement(data);
                    localStorage.setItem('data', JSON.stringify(data));

                    var qArray = Object.keys(data.questions);
                    this.localStorage.setItem('qArray', JSON.stringify(qArray));
                    localStorage.setItem('qcounter', 0);
                    localStorage.setItem('total', qArray.length);

                });

        }
    } catch (error) {

    }


    // $("*[contenteditable='true']").attr("contenteditable",false);
}

function displayAssesement(assesement) {
    $('#asses-desc').html(assesement.assesemenDesc);
    $('body').css('background-image', `url(${assesement.assesementImage})`);
}

// function updateAssesement(assesementId) {
//     var assesementDesc = 'Which European City Could You Visit During the Eurovision'; // wil be done once per template
//     var validOptions = [];

//     // Below Function for Valid Options will need  ... (name, description, image)
//     validOptions.push(new validOption('Barcelona', 'From the mountains to the beach, the historic to the contemporary, sunny Barcelona has it all.', 'https://cdn.filestackcontent.com/4pdcjqEHRB2kwVwcWq5Z'));
//     validOptions.push(new validOption('Geneva', 'From the mountains to the beach, the historic to the contemporary, sunny Barcelona has it all.', 'https://cdn.filestackcontent.com/4pdcjqEHRB2kwVwcWq5Z'));
//     validOptions.push(new validOption('Zurich', 'From the mountains to the beach, the historic to the contemporary, sunny Barcelona has it all.', 'https://cdn.filestackcontent.com/4pdcjqEHRB2kwVwcWq5Z'));
//     validOptions.push(new validOption('Vienna', 'From the mountains to the beach, the historic to the contemporary, sunny Barcelona has it all.', 'https://cdn.filestackcontent.com/4pdcjqEHRB2kwVwcWq5Z'));

//     var jsonObj = {};
//     jsonObj.assesemenDesc = assesementDesc;
//     jsonObj.validOptions = validOptions;
//     jsonObj.validOptions.heading = 'Here is your dream destination';

//     db_ref.ref("/assesement/" + assesementId).update(jsonObj);
// }



// function editQuestion(assesementId, questionId) {
//     var questionText = 'Pick Your Travel Budget';
//     var questionImage = 'https://cdn.filestackcontent.com/SRyNyKSkQG5JOd8iLxO1';
//     var options = [];

//     options.push(new option('$150', '["4", "1", "6", "3"]'));
//     options.push(new option('$1500', '["4", "2", "6", "3"]'));
//     options.push(new option('$15000', '["4", "3", "6", "3"]'));
//     options.push(new option('$150000', '["4", "4", "6", "3"]'));

//     var obj = new Question(questionText, questionImage, options);
//     console.log(obj);

//     db_ref.ref("/assesement/" + assesementId + "/questions/" + questionId).update(obj);
// }