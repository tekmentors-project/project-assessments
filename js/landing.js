//Get the question w.r.t current counter and increase counter by 1;
function displayQuestion() {
    var data = JSON.parse(localStorage.getItem('data'));
    var qArray = JSON.parse(localStorage.getItem('qArray'));
    var counter = parseInt(localStorage.getItem('qcounter'));
    var question = data.questions[qArray[counter]];
    if(null === question){
       ++counter;
       question = data.questions[qArray[counter]];
    }
    localStorage.setItem('qcounter', counter + 1);
    console.log("question: "+question);
    return question;
}


function updateResultDisplay() {
    var data = JSON.parse(localStorage.getItem('data'));
    var optionsArray = data.validOptions;

    var weightageArray = JSON.parse(localStorage.getItem('weightage'));
    var indexOfMaxValue = weightageArray.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);
    var resultObj = optionsArray[indexOfMaxValue];

    $("#result-heading").html(data.resultHeading);
    $("#result-image").attr('src', resultObj.image);
    $("#result-name").html(resultObj.name);
    $("#result-desc").html(resultObj.description);

}

function updateQuestionDisplay() {
    var question = displayQuestion();
    if(null === question){
        question = displayQuestion();
    }
    $(".ques-pic img").attr('src', question.questionImage);
    $(".ques-div h4").html(question.questionText);
    $("#opt4").html(question.options[0].text);
    $("#opt1").html(question.options[1].text);
    $("#opt2").html(question.options[2].text);
    $("#opt3").html(question.options[3].text);
}

function calculateWeightage(optionId) {
    var data = JSON.parse(localStorage.getItem('data'));
    var qArray = JSON.parse(localStorage.getItem('qArray'));
    var counter = localStorage.getItem('qcounter');
    var question = data.questions[qArray[counter - 1]];
    var option = question.options[parseInt(optionId)];
    console.log('questionId: ' + qArray[counter] + '  optionId: ' + optionId);
    var currentWeightage = option.weightage;
    console.log('currentWeightage: ' + currentWeightage);

    var previousWeightage = localStorage.getItem('weightage');
    if (null == previousWeightage || undefined == previousWeightage || "undefined"== previousWeightage) {
        localStorage.setItem('weightage', option.weightage);
    } else {
        var previousWtgArray = JSON.parse(previousWeightage);
        var currentWtgArray = JSON.parse(currentWeightage);
        var previousWtgArray = currentWtgArray.map(function (num, idx) {
            return parseInt(num) + parseInt(previousWtgArray[idx]);
        });
        console.log("updated Weightage: " + previousWtgArray);
        localStorage.setItem('weightage', "[" + previousWtgArray + "]");
    }
}

function removeModal(){
    event.preventDefault();
    $('#modal-div').modal('hide');
}

function redirectToIndex(){
    event.preventDefault();
    window.location="index.html";
}

$(document).ready(function () {
    $("#question-div").hide();
    $("#modal-div").hide();
    $("#result-div").hide();

    $("#btn-started").click(function () {
        localStorage.setItem('qcounter', 0);
        localStorage.removeItem('weightage');
        updateQuestionDisplay();
        $("#started-div").hide();
        $("#question-div").show();

    });

    $('#ques-nxt').click(function () {
        calculateWeightage($('input[name=customRadioQ1]:checked').val());
        $('#ques-nxt').prop("disabled", true);
        $('input[type=radio]').prop('checked', function () {
            return this.getAttribute('checked') == 'checked';
        });

        if (localStorage.getItem('qcounter') < localStorage.getItem('total')) {
            updateQuestionDisplay();
        } else {
            $("#question-div").hide();
            $("#result-div").show();
            updateResultDisplay();
            $("#modal-div").modal('toggle');
        }
    });

    $('.ques-radio').click(function () {
        $('#ques-nxt').prop("disabled", false);
    });

    $('#modal-cont').click(function () {
        var inputName = $('#inputName').val();
        var inputEmail = $('#inputEmail').val();

        if (null != inputName && null != inputEmail) {

            var data = JSON.parse(localStorage.getItem('data'));
            var assesementId = data.assesementId;
            var optionsArray = data.validOptions;

            var weightageArray = JSON.parse(localStorage.getItem('weightage'));
            var indexOfMaxValue = weightageArray.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);
            console.log(optionsArray[indexOfMaxValue]);
            var assesResult = optionsArray[indexOfMaxValue].name;
            saveResult(assesementId, inputName, inputEmail, assesResult);
        }
    });

});