$(document).ready(function () {
    $('#displayContent').load('templatesHome.html', function () {
        loaded();
    });

    function carouselHover(){
        $(".carousel-main").mouseover(function(){
            document.getElementById(this.id).setAttribute("data-interval",true);
        });
    }
    
    function loaded() {
        $('.carousel-main').carousel();
        carouselHover();
        $("#linkToTemp1").bind('click', function () {
            var user = firebase.auth().currentUser;
            console.log(user);
            if (user) {
                createAssesement().then(() => {
                    window.location = 'quizEditor.html';
                    console.log("got assessment id");
                });
            } else {
                $('#displayContent').load('login.html');
            }
        });
    }
    $("#registration").bind("click", function () {
        $('#displayContent').load('login.html');
    });
    
})